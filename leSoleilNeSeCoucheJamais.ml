open Unix
open Lwt
open Lwt_unix
module Irc = Irc_client_lwt


let villes = [
  ["Pasadena", ["Pasadena"]; "Palo Alto", ["Palo Alto"]]               , -7 ;
  ["Boulder", ["Boulder"]]                             , -6 ;
  ["New York", ["New York"]]                            , -4 ;
  ["Cambridge", ["Cambridge" ; "UK"]]                           ,  1 ;
  ["Paris", ["Paris"]; "Gand", ["Ghent"]; "Århus", ["Aarhus"]; "Göteborg", ["Goteborg"]] ,  2 ;
  ["Kyoto", ["Kyoto"]; "Tokyo", ["Tokyo"]]                      ,  9
]

let rec combine l =
  match l with
  | [] -> ""
  | [x] -> x
  | [x ; y] -> x ^ " et " ^ y
  | x :: t -> x ^ ", " ^ combine t

let expr tm (villes, dec) =
  Printf.sprintf "%dh%02d %s"
    ((tm.tm_hour + 24 + dec) mod 24) tm.tm_min
    (villes |> List.map fst |> List.map ((^) "à ") |> combine)

let donne_heure tm villes =
  Printf.sprintf "Il est %s. Le soleil ne se couche jamais !"
  @@ combine (List.map (expr tm) villes)

let donne_meteo villes =
  List.map fst villes |> List.flatten
  |> Lwt_list.map_s
    (fun (nom, id) ->
      Weather.meteo id >>= fun (m, t) -> return (Printf.sprintf "%s à %s (%.1f°C)" m nom t))
  >>= fun l -> return @@ Printf.sprintf "Météo actuelle : %s" (combine l)

(* C'est juste une blague... Que celui qui l'utilise assume ses responsabilités
  *)
let mail_xxx mailer =
  let to_addrs = Secrets.mails in
  let from_addr = ( "#info14", "croisilloninfo14@ulminfo.fr") in
  let subject = "Sans nouvelles" in
  let body =
    "Salut,\n" ^
    "Ça fait longtemps qu'on a pas eu de nouvelles. Qu'est-ce que tu deviens ?\n\n" ^
    mailer ^ ", avec le soutien de toute la promo"
  in
  let email =
    Netsendmail.compose
      ~from_addr ~to_addrs
      ~in_charset:`Enc_utf8 ~out_charset:`Enc_utf8
      ~subject body
  in return @@ Netsendmail.sendmail ~mailer:"/usr/sbin/sendmail" email


let donne_aide options =
  "Ordres spécifiques acceptés:" ::
  (List.map (fun (opts, _, com) -> combine opts ^ ": " ^ com) options)

let debug = false

let server = "ulminfo.fr"
let port = 3724

let realname =
  if debug
  then "Le Soleil ne se couche jamais Bot, Debug Gold Edition, Version 12"
  else "Le Soleil ne se couche jamais Bot, 5th Edition, Version 0.447903"

let nick =
  if debug
  then "lsnscjbd"
  else "lsnscjb"

let username = nick

let should_quit = ref false

let channel =
  if debug
  then "#test"
  else "#info14"

let target =
  if debug
  then "Ctl-Maj-5cfb"
  else channel



let unwrap b =
  match b with
  | Some x -> x
  | None -> failwith "No value"

let send_message connection message =
  Irc.send_privmsg ~connection ~target ~message


let start_with s subs =
  try String.sub s 0 (String.length subs) = subs
  with Invalid_argument _ -> false

let ordre_specifique opts msg =
  try
    let (_, f, _) =
      (List.find
         (fun (l,_,_) -> List.exists (fun s -> msg = username ^ ": " ^ s) l)
         opts)
    in
    Some f
  with Not_found -> None

let ping_server connection =
  let open Irc_message in
  Irc.listen ~connection ~callback:(
    fun connection result ->
    match result with
    | `Ok ({ command = PRIVMSG (_, msg) ; prefix })->
      let options =
        [
          ["va dormir !"],
          (fun _ -> Irc.send_quit ~connection),
          "Tue lsnscjb" ;

          ["ii otenki desu ne"; "truc en japonais"],
          (fun _ -> donne_meteo villes >>= send_message connection),
          "Montre que la pluie est une constante universelle" ;

          ["d'ailleurs on a des nouvelles d'alban ?"],
          (fun _ -> mail_xxx (unwrap prefix)),
          "..." ;


          ["à l'aide !"],
          (fun options ->
             donne_aide options
             |> Lwt_list.fold_left_s (fun _ -> send_message connection) ()),
          "Ne fais pas grand chose de plus" ;
        ]
      in
      let msg = String.trim msg in
      begin
        match ordre_specifique options msg with
        | Some f -> f options
        | None when msg = username || start_with msg (username ^ ":") ->
          let message = donne_heure (gmtime @@ time ()) villes
          in send_message connection message
        | None -> return ()
      end
    | `Ok _ -> return ()
    | `Error e ->
      Lwt_io.printl e
  )


let lwt_main () =
  Irc.connect_by_name ~server ~port ~username ~mode:0 ~realname ~nick ()
  >>= fun connection -> return (unwrap connection)
  >>= fun connection -> return (Lwt_daemon.daemonize ())
  >>= fun () -> sleep 20.0
  >>= fun () -> Irc.send_join ~connection ~channel
  >>= fun () -> ping_server connection

let _ =
  Lwt_main.run (lwt_main ())



(*
let _ =
  Lwt_main.run (donne_meteo villes >>= Lwt_io.printl >>= Lwt_io.flush_all)
*)
