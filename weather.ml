open Lwt
open Cohttp
open Cohttp_lwt_unix

module Json = Yojson.Basic

let translate_weather = function
  | "Clear" -> "ciel dégagé"
  | "Rain" -> "pluie"
  | "Clouds" -> "nuageux"
  | "Thunderstorm" -> "orageux"
  | "Haze" -> "brume sèche"
  | "Fog" -> "brouillard"
  | "Mist" -> "brumeux"
  | m -> "\"" ^ m ^ "\""


let parse_info body =
  print_endline body ;
  let json = Json.from_string body in
  let open Json.Util in
  let weather = json |> member "weather" |> index 0 |> member "main" |> to_string |> translate_weather in
  let temp = json |> member "main" |> member "temp" |> to_number in
  return (weather, temp)


let meteo ville =
  let uri =
    Uri.with_query
      (Uri.of_string "http://api.openweathermap.org/data/2.5/weather")
      ["q", ville ; "appid", [Secrets.appid] ; "units", ["metric"]]
  in
  Client.get uri
  >>= fun (_, body) -> Cohttp_lwt_body.to_string body
  >>= parse_info
