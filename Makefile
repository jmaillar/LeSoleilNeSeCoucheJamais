

all: leSoleilNeSeCoucheJamais.native

leSoleilNeSeCoucheJamais.native: leSoleilNeSeCoucheJamais.ml weather.ml secrets.ml
	ocamlbuild -use-ocamlfind -pkgs irc-client.lwt,cohttp.lwt,yojson,ssl,netsys,netstring,netclient leSoleilNeSeCoucheJamais.native

clean:
	ocamlbuild -clean

.PHONY: leSoleilNeSeCoucheJamais.native
